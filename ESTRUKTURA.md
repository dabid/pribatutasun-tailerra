# Pribatutasun tailerra

1. Hack.in#badakigu aurkezpena (2 min).
1. Pribatutasun hitzaldia (oinarri teorikoak ezartzeko hitzaldi txikia)
  * Zergatik egiten dugu hitzaldi hau?
    * Erabaki politiko bat.
    * Datuen motxila daukagu.
    * Pribatutasunaren bukaera.
  * Nori zuzenduta dago?
  * Segurtasun operazionala.
  * Segurtasun instrumentala.
1. Segurtasun instrumentalean sakontzen.

## Garatzeko eta eztabaidatzeko oharrak

* Weareableen inguruan berba egin behar dugu.
