# Pribatutasun tailerra

Biltegi honetan, hack.in#badakigu informatika elkartearen pribatutsun tailerra aurkituko duzue.

### hack.in#badakigu informatika elkarte asketa

![hack.in#badakigu informatika elkarte asketa](https://hackinbadakigu.net/wp-content/uploads/2016/12/hackinbadakigu-logo.png)

Hack.in#badakigu informatika elkarte askea, herritik eta herriarentzat sortutako erakunde tekno-politiko, aurrerakoi, euskaldun eta euskaltzalea gara. 10 urtetik gora daramagu jendartearen garapenean eta giza eskubideen inguruan lanean ari diren norbanako zein eragileak ahalduntzen teknologia askeak erabiliz, azken hauek eraldaketa sozialerako ezinbesteko tresnak direlako.

Informazio gehiago: https://hackinbadakigu.net/

## Softwarea

Aurkezpen hau [reveal.js](https://revealjs.com/) software librearekin eginda dago.

## Parte hartu dute

Eskerrik asko aurkezpen hau zuon parte hartzearekin posible egin duzuen guztioi:

* Dabid Martinez ([@dabidnet@mastodon.eus](https://mastodon.eus/@dabidnet)).
